

$(document).ready(function () {


	$(".wrap-icon-toggle").click(function () {
		$(this).toggleClass('active');
		$('body').toggleClass('overlay');
		$('nav#navs').toggleClass('active');
		$('header#header .logo').toggleClass('active-menu');
		$('main#main').toggleClass('active-menu');
		$('footer#footer').toggleClass('active-menu');
	});
	$(document).mouseup(function (e) {
		var container = $("nav#navs, header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$('.wrap-icon-toggle').removeClass('active');
				$('body').removeClass('overlay');
				$('nav#navs').removeClass('active');
				$('header#header .logo').removeClass('active-menu');
				$('main#main').removeClass('active-menu');
				$('footer#footer').removeClass('active-menu');
		}
	});

	$(".toggle-menu").click(function () {
		$(this).toggleClass('active');
		$(this).siblings('.menu-child').slideToggle();
	});

});

$('#slide-application').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	margin: 20,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
		},
		540: {
			items: 2,
			margin:20
		},
		768: {
			items: 3,
			margin:20
		},
		1024: {
			items: 4,
			margin: 30
		},
	}
});
$('#slide-certi, .slide-banner-main').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	margin: 10,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
		}
	}
});


// fix header 
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-190px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header:not(.header-project-detail-not-scroll)').addClass('active');
	} else {
		$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
	};
});


